import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

fig = plt.figure()
ax = fig.gca(projection='3d')

x1 = np.arange(0, 2 * np.pi, 0.1)
y1 = np.sin(x1)
ax.plot(x1, y1, zs=0, zdir='z', label='curve in (x1,y1)')

x2 = np.arange(0, 2 * np.pi, 0.1)
z2 = np.sin(x2)
ax.plot(x2, z2, zs=0, zdir='y', label='curve in (x2,z2)')

y3 = np.arange(0, 2 * np.pi, 0.1)
z3 = np.sin(y3)
ax.plot(y3, z3, zs=0, zdir='x', label='curve in (y3,z3)')

ax.set_xlim(0, 4)
ax.set_ylim(-1, 4)
ax.set_zlim(-1, 1)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')

plt.show()